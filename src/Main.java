class EmptyDataException extends Exception {}
class Main {
    private int getMaxFromDigits(final String digits) throws EmptyDataException {
        // prosze zaimplementowac algorytm wyszukiwania max z podanego ciagu cyfr
        // naglowek metody nie moze ulec zmianie!
        String[] splittedDigits = null;
        splittedDigits = digits.split(",");
        int result = Integer.parseInt(splittedDigits[0]);
        for(String digit : splittedDigits ) {
            if(Integer.parseInt(digit) > result){
                result = Integer.parseInt(digit);
            }
        }

        return result;
    }
    // rzuca wyjatek EmptyDataException gdy parametr wejsciowy jest pusty
    private int getMinFromDigits(final String digits) throws EmptyDataException {
        // prosze zaimplementowac algorytm wyszukiwania min z podanego ciagu cyfr
        // naglowek metody nie moze ulec zmianie!
        String[] splittedDigits = null;

        splittedDigits = digits.split(",");
        int result = Integer.parseInt(splittedDigits[0]);
        for(String digit : splittedDigits ) {
            if(Integer.parseInt(digit) < result){
                result = Integer.parseInt(digit);
            }
        }

        return result;
    }
    public static void main(String[] args) {
        final String listOfDigits = "1,2,3,4,5,5,6,10,5,4,2,1";
        final Main main = new Main();
        try {
            System.out.println("Wartosc max: " + main.getMaxFromDigits(listOfDigits));
            System.out.println("Wartosc min: " + main.getMinFromDigits(listOfDigits));
            // wywolanie funkcji oraz wyswietlenie wyniku na standardowe wyjscie...
        } catch(EmptyDataException e) {
            System.out.println("Lista nie moze byc pusta!");
        }
    }
}